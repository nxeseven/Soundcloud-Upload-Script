import soundcloud

client = soundcloud.Client(
    client_id='*',
    client_secret='*',
    username='*@gmail.com',
    password='*'
    )
    
track = client.post('/tracks', track={
    'title': 'NPR Kenny G Remix',
    'genre': 'Triphop',
    'tag_list': "morningeditiontheme\" geo:lat=32.444 geo:lon=55.33",
    'description': 'A remix of "kenny G-Inspired Theme" from this link:
http://www.npr.org/2016/04/26/467352425/whats-your-take-on-morning-editions-theme',
    'sharing': 'private',
    'license': 'creative-commons',
    'downloadable': True,
    'bpm': 106.998,
    'asset_data': open('NPR_Kenny_G_Remix.wav', 'rb'),
    'artwork_data': open('palmsky.jpg','rb')
#'tag_list': "tag1 \"morningeditiontheme\" geo:lat=32.444 geo:lon=55.33",
})

print(track.title)
